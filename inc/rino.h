/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#ifndef RINO_H
#define RINO_H

////////////////////////////////////////////////////////////////////////////////
/// INCLUDES
////////////////////////////////////////////////////////////////////////////////

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "lauxlib.h"
#include "luajit.h"
#include "lualib.h"

#include "basics.h"
#include "string.h"
#include "lst.h"
#include "vec.h"

////////////////////////////////////////////////////////////////////////////////
/// DEFINES
////////////////////////////////////////////////////////////////////////////////

#define E				2.7182818284

#define PORT_SCLANG		57120
#define PORT_SCSYNTH	57110

#define BUFFER_ID		64
#define BUFFER_CALL		256
#define BUFFER_PAT		128
#define BUFFER_PAT_DATA	128

#define BPM_DEFAULT		60
#define BEAT_PRECISION	0.001
#define BEAT_ROUND(t)	((double)((int)((t) * 1000.0)) / 1000.0)	// round ?

#define PARAM_INT			'i'
#define PARAM_FLOAT			'f'
#define PARAM_STRING		's'
#define EVENT_CALL			0
#define EVENT_WAIT			1
#define EVENT_WAIT_REGULAR	0
#define EVENT_WAIT_FOR		1
#define PATTERN_REGULAR		0
#define PATTERN_RECURSIVE	1
#define PATTERN_SEQ			0
#define PATTERN_NSEQ		1
#define PATTERN_RAND		2
#define PATTERN_XRAND		3
#define PATTERN_WRAND		4
#define PATTERN_SHUF		5
#define PATTERN_NSHUF		6

////////////////////////////////////////////////////////////////////////////////
/// TYPEDEFS
////////////////////////////////////////////////////////////////////////////////

typedef struct sockaddr_storage	t_sockaddr_storage;
typedef struct sockaddr_in		t_sockaddr_in;
typedef struct sockaddr			t_sockaddr;
typedef struct timeval			t_time;
typedef struct tm				t_time_mod;

typedef struct s_pool			t_pool;
typedef struct s_call_param		t_call_param;
typedef struct s_call			t_call;
typedef struct s_wait			t_wait;
typedef struct s_event			t_event;
typedef struct s_pattern		t_pattern;
typedef struct s_loop			t_loop;
typedef struct s_rino			t_rino;

////////////////////////////////////////////////////////////////////////////////
/// STRUCTURES
////////////////////////////////////////////////////////////////////////////////

struct							s_pool {
	size_t						item_size;
	t_list						*pool;
};

struct							s_call_param {
	char						type;				// Integer | Float | String
	union {
		i32						i32;				// Integer value
		float					f32;				// Float value
		char					str[BUFFER_ID];		// String value
	}							value;
};

struct							s_call {
	char						path[BUFFER_ID];	// Call path
	t_list						*param;				// Call parameters
};

struct							s_wait {
	u8							type;				// Wait type
	double						beat;				// Aimed beat
	bool						last;				// Is last queued delay
};

struct							s_event {
	u8							type;				// Call | Delay
	union {
		t_call					call;				// Call  - call value
		t_wait					wait;				// Delay - delay value
	}							content;
};

struct							s_pattern {
	int							buffer[BUFFER_PAT_DATA];// i32 | f32 data buffer
	u8							type;					// Pattern type
	u8							mode;					// Pattern mode
	int							value;
	int							length;					// Pattern child length
	int							count;					// Pattern count
	int							a;
	int							b;
};

struct							s_loop {
	char						id[BUFFER_ID];		// Loop id
	double						beat;				// Loop time cursor
	t_list						*queue;				// Loop events queue
	bool						error;				// Loop got error
	struct {
		t_pattern				list[BUFFER_PAT];
		int						i;
	}							pattern;
};

struct							s_rino {
	char						path[BUFFER_ID];	// File path
	struct {
		t_sockaddr_in			server;				// Osc socket server
		int						client_socket;		// Osc socker client
	}							osc;
	struct {
		double					bpm;				// Playing bpm
		double					beat;				// Actual beat
		//double					beat_wait;
		bool					new_beat;
		t_time					time_start;
		t_time_mod				update;
	}							time;
	struct {
		t_pool					loop;
		t_pool					event;
		t_pool					call_param;
	}							pool;
	t_list						*loops;				// Loops
	t_list						*loops_prev;		// Loops before update
	t_loop						*loop;				// current loop
};

t_rino		r;
lua_State	*l;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

void	pool_init(t_pool *pool, size_t item_size);
t_list	*pool_get(t_pool *pool);
void	pool_release(t_pool *pool, t_list *item);
void	pool_release_end(t_pool *pool, t_list *item);
void	pool_free(t_pool *pool, void (*func)(void *));
void	endian_big(void *value, int size);
double	random_range(double min, double max);

bool	lua_init(void);
void	lua_init_api(void);
void	lua_init_scales(void);
bool	lua_load_file(void);
void	server_init(void);

int		api_wait(lua_State *l);
int		api_wait_for(lua_State *l);
int		api_time(lua_State *l);
int		api_play(lua_State *l);
int		api_scplay(lua_State *l);
int		api_midi_cps(lua_State *l);
int		api_cps_midi(lua_State *l);
int		api_pattern(lua_State *l);
int		api_pseq(lua_State *l);
int		api_prand(lua_State *l);
int		api_pxrand(lua_State *l);
int		api_pwrand(lua_State *l);
int		api_pshuf(lua_State *l);
int		api_rand(lua_State *l);
int		api_wrand(lua_State *l);
int		api_exprand(lua_State *l);
int		api_floor(lua_State *l);
int		api_ceil(lua_State *l);
int		api_round(lua_State *l);
int		api_pow(lua_State *l);
int		api_sqrt(lua_State *l);
int		api_sin(lua_State *l);
int		api_cos(lua_State *l);
int		api_lerp(lua_State *l);
int		api_map(lua_State *l);
int		api_constrain(lua_State *l);
int		api_max(lua_State *l);
int		api_min(lua_State *l);
int		api_abs(lua_State *l);

bool	loop_new(char *id);
void	loop_free(t_loop *loop);
bool	loop_compute(t_loop *loop);

bool	call_param_new_int(t_call *call, int value);
bool	call_param_new_float(t_call *call, float value);
bool	call_param_new_string(t_call *call, char *value);
void	call_make(t_call *call);
void	call_free(t_call *call);

#endif

