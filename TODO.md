
https://depts.washington.edu/dxscdoc/Help/Reference/Server-Command-Reference.html

# ERROR

# API

- [ ] Timing
	- [x] `wait()`					Wait amount of time
	- [x] `wait_for(i)`				Wait for beat mod i
	- [ ] `align(i)`				Loop start or re-start aligned on beat mod i
- [x] Play
	- [x] `play(path, {"key", value})`			Generic OSC call
	- [x] `scplay("synth", {"key", value})`		Supercollider oriented OSC call
- [x] Math
	- [x] `rand()`
	- [x] `rand(hi)`
	- [x] `rand(lo, hi)`
	- [x] `rand(list)`
	- [x] `wrand(list, weight)`
	- [x] `exprand(hi)`
	- [x] `exprand(lo, hi)`
	- [x] `exprand(list)`
	- [x] sin, cos, min, max, lerp, map, ...
- [x] Conversion
	- [x] `midi_cps(midi, [440])`
	- [x] `cps_midi(cps, [440])`
	- [ ] `midi_ratio(midi)`
- [ ] Pattern
	- [x] `pseq({a, b, c})`					Sequence selection
	- [x] `prand({a, b, c})`				Random selection
	- [x] `pxrand({a, b, c})`				Random selection, avoid last selected item
	- [x] `pwrand({a, b, c}, {wa, wb, wc})`	Weighted random selection
	- [x] `pshuf({a, b, c})`				Shuffled sequence selection
	- [ ] `pattern({{{}}})`
		- [x] {"seq", n, {1, 2, 3}}
		- [x] {"rand", n, {1, 2, 3}}
		- [x] {"xrand", n, {1, 2, 3}}
		- [x] {"wrand", n, {1, 2, 3}, {...}}
		- [x] {"shuf", n, {1, 2, 3}}
		- [x] {"nseq", n, {1, 2, 3}}		with `n` single outputs
		- [x] {"nshuf", n, {1, 2, 3}}
		- [ ] {"white", n, lo, hi}
		- [ ] {"exp", n, lo, hi}
		- [ ] {"ln", n, lo, hi}

# Data

- [x] Scales

# Metatables

- [ ] Table operations ({60, 63, 65} + 4, {60, 63, 65} / 2)

# Ease

- [ ] `ease(_G, {"amp", 1})`	ease value in time
