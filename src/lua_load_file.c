/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static bool
del_loop(t_list *node, void *param) {
	t_loop	*loop;

	loop = (t_loop*)&(node->content);
	lua_pushnil(l);
	lua_setglobal(l, loop->id);
	param = NULL;
	return false;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool
lua_load_file(void) {
	struct stat	stats;
	char		*key;

	/// DEL USER FUNCTIONS (LUA)
	lst_iter(&(r.loops), (char(*)(t_list*,void*))del_loop, NULL);
	/// RUN USER FILE (LUA)
	if (luaL_dofile(l, r.path) != 0)
		return false;
	/// LOAD USER FUNCTIONS (LUA TO C)
	r.loops_prev = r.loops;
	r.loops = NULL;
	lua_getglobal(l, "_G");
	if (lua_istable(l, -1) == true) {
		lua_pushnil(l);
		/// KEY		index - 2
		/// VALUE	index - 1
		while (lua_next(l, -2) != 0) {
			if (lua_isfunction(l, -1) == true) {
				key = (char*)lua_tostring(l, -2);
				if (str_nequ(key, "loop_", 5) == true)
					loop_new(key);
			}
			lua_pop(l, 1);
		}
	}
	/// FREE UNUSED USER FUNCTIONS (C)
	lst_del_ex(&(r.loops_prev), (void(*)(void*))loop_free);
	/// UPDATE TIME
	if (stat(r.path, &stats) == 0)
		r.time.update = *(gmtime(&stats.st_mtime));
	else
		return false;
	return true;
}
