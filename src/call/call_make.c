/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline int
call_push_string(byte *buffer, char *string) {
	int				i;

	/// PUSH STRING
	i = 0;
	while (string[i] != 0) {
		buffer[i] = string[i];
		i += 1;
	}
	buffer[i] = 0;
	/// ROUND ZERO
	i += 1;
	while (i % 4 != 0) {
		buffer[i] = 0;
		i += 1;
	}
	return i;
}

static inline int
call_push_int(byte *buffer, int number) {
	*((int*)buffer) = number;
	endian_big(buffer, 4);
	return 4;
}

static inline int
call_push_float(byte *buffer, float number) {
	*((float*)buffer) = number;
	endian_big(buffer, 4);
	return 4;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
call_make(t_call *call) {
	byte			buffer[BUFFER_CALL];
	t_call_param	*param;
	t_list			*list;
	int				i;

	/// PUSH OSC PATH
	i = call_push_string(buffer, call->path);
	/// PUSH OSC TYPES
	buffer[i] = ',';
	i += 1;
	list = call->param;
	while (list != NULL) {
		param = (t_call_param*)&(list->content);
		buffer[i] = param->type;
		i += 1;
		list = list->next;
	}
	buffer[i] = 0;
	i += 1;
	while (i % 4 != 0) {
		buffer[i] = 0;
		i += 1;
	}
	/// PUSH OSC PARAMETERS
	list = call->param;
	while (list != NULL) {
		param = (t_call_param*)&(list->content);
		if (param->type == PARAM_INT)
			i += call_push_int(buffer + i, param->value.i32);
		else if (param->type == PARAM_FLOAT)
			i += call_push_float(buffer + i, param->value.f32);
		else if (param->type == PARAM_STRING)
			i += call_push_string(buffer + i, param->value.str);
		list = list->next;
	}
	/// SEND OSC
	sendto(r.osc.client_socket,
	/**/ buffer, i,
	/**/ 0, (t_sockaddr*)&r.osc.server, sizeof(t_sockaddr_in));
}
