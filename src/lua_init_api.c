/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
lua_init_api(void) {
	lua_register(l, "play", api_play);
	lua_register(l, "scplay", api_scplay);
	lua_register(l, "wait", api_wait);
	lua_register(l, "wait_for", api_wait_for);
	lua_register(l, "time", api_time);

	lua_register(l, "midi_cps", api_midi_cps);
	lua_register(l, "cps_midi", api_cps_midi);

	lua_register(l, "pattern", api_pattern);
	lua_register(l, "p", api_pattern);
	lua_register(l, "pseq", api_pseq);
	lua_register(l, "prand", api_prand);
	lua_register(l, "pxrand", api_pxrand);
	lua_register(l, "pwrand", api_pwrand);
	lua_register(l, "pshuf", api_pshuf);

	lua_register(l, "rand", api_rand);
	lua_register(l, "wrand", api_wrand);
	lua_register(l, "exprand", api_exprand);

	lua_register(l, "floor", api_floor);
	lua_register(l, "ceil", api_ceil);
	lua_register(l, "round", api_round);
	lua_register(l, "pow", api_pow);
	lua_register(l, "sqrt", api_sqrt);
	lua_register(l, "sin", api_sin);
	lua_register(l, "cos", api_cos);
	lua_register(l, "min", api_min);
	lua_register(l, "max", api_max);
	lua_register(l, "abs", api_abs);
	lua_register(l, "lerp", api_lerp);
	lua_register(l, "map", api_map);
	lua_register(l, "constrain", api_constrain);
}
