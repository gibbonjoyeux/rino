/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		pool_release(t_pool *pool, t_list *item) {
	if (item != NULL) {
		item->next = pool->pool;
		pool->pool = item;
	}
}
