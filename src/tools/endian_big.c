/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline int
is_big_endian(void) {
    int		test;
    char	*p;

	test = 1;
	p = (char*)&test;
    return p[0] == 0;
}

static inline void
reverse_endian(void *value, int size){
    int		i;
    char	result[32];

    for(i = 0; i < size; i += 1)
        result[i] = ((char*)value)[size - i - 1];
    for(i = 0; i < size; i += 1)
        ((char*)value)[i] = result[i];
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
endian_big(void *value, int size){
    char	to_fix;

	to_fix = !((1 && is_big_endian()) || (0 && !is_big_endian()));
    if (to_fix == true)
        reverse_endian(value, size);
}
