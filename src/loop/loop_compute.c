/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static bool
loop_run_function(t_loop *loop) {
	int		err;

	if (loop->error == true)
		return false;
	/// INIT LOOP
	loop->pattern.i = 0;
	/// RUN LOOP
	lua_getglobal(l, loop->id);
	err = lua_pcall(l, 0, 0, 0);
	if (err != 0) {
		loop->error = true;
		/// PRINT ERROR
		printf("[%s] %s\n", loop->id, lua_tostring(l, -1));
		lua_pop(l, 1);
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool
loop_compute(t_loop *loop) {
	t_list		*node;
	t_event		*event;

	r.loop = loop;
	while (true) {
		/// GET EVENT
		if (loop->queue == NULL)
			if (loop_run_function(loop) == false || loop->queue == NULL)
				return false;
		event = (t_event*)&(loop->queue->content);
		/// EVENT CALL
		if (event->type == EVENT_CALL) {
			call_make(&(event->content.call));
			call_free(&(event->content.call));
		/// EVENT WAIT
		} else if (event->type == EVENT_WAIT) {
			if (r.time.beat <= event->content.wait.beat) {
				if (event->content.wait.last == true)
					return loop_run_function(loop);
				return true;
			}
		}
		/// POP EVENT
		node = loop->queue;
		loop->queue = loop->queue->next;
		pool_release(&(r.pool.event), node);
	}
	return true;
}
