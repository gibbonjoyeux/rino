/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

bool
loop_new(char *id) {
	t_list	*node, *node_prev;
	t_loop	*loop;

	/// CHECK LOOP
	node = r.loops_prev;
	node_prev = NULL;
	while (node != NULL) {
		loop = (t_loop*)&(node->content);
		/// LOOP ALREADY EXISTS
		if (str_equ(loop->id, id) == true) {
			if (node_prev == NULL)
				r.loops_prev = node->next;
			else
				node_prev->next = node->next;
			loop->error = false;
			lst_push(&(r.loops), node);
			return true;
		}
		node_prev = node;
		node = node->next;
	}
	/// ALLOC LOOP
	node = pool_get(&(r.pool.loop));
	if (node == NULL)
		return false;
	loop = (t_loop*)&(node->content);
	/// INIT LOOP
	mem_clean(loop, sizeof(t_loop));
	str_cpy(loop->id, id);
	loop->beat = (int)r.time.beat;
	loop->error = false;
	/// PUSH LOOP
	lst_push(&(r.loops), node);
	return true;
}
