/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
new_scale(char *name, double *scale) {
	int			i;

	lua_newtable(l);
	i = 0;
	while (scale[i] >= 0) {
		lua_pushnumber(l, scale[i]);
		lua_rawseti(l, -2, i + 1);
		i += 1;
	}
	lua_setfield(l, -2, name);
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
lua_init_scales(void) {
	lua_newtable(l);

	new_scale("minorPentatonic", (double[]){0,3,5,7,10,-1});
	new_scale("majorPentatonic", (double[]){0,2,4,7,9,-1});
	//anothermodeofmajorpentatonic
	new_scale("ritusen", (double[]){0,2,5,7,9,-1});
	//anothermodeofmajorpentatonic
	new_scale("egyptian", (double[]){0,2,5,7,10,-1});

	new_scale("kumoi", (double[]){0,2,3,7,9,-1});
	new_scale("hirajoshi", (double[]){0,2,3,7,8,-1});
	new_scale("iwato", (double[]){0,1,5,6,10,-1});
	new_scale("chinese", (double[]){0,4,6,7,11,-1});
	new_scale("indian", (double[]){0,4,5,7,10,-1});
	new_scale("pelog", (double[]){0,1,3,7,8,-1});

	new_scale("prometheus", (double[]){0,2,4,6,11,-1});
	new_scale("scriabin", (double[]){0,1,4,7,9,-1});

	//hanchinesepentatonicscales
	new_scale("gong", (double[]){0,2,4,7,9,-1});
	new_scale("shang", (double[]){0,2,5,7,10,-1});
	new_scale("jiao", (double[]){0,3,5,8,10,-1});
	new_scale("zhi", (double[]){0,2,5,7,9,-1});
	new_scale("yu", (double[]){0,3,5,7,10,-1});

	//6notescales
	//new_scale("whole"new((0,2..10),12,name:"WholeTone"),
	new_scale("augmented", (double[]){0,3,4,7,8,11,-1});
	new_scale("augmented2", (double[]){0,1,4,5,8,9,-1});

	/*
	//Partch'sOtonalitiesandUtonalities
	\partch_o1->Scale.new(#[0,8,14,20,25,34],43,
	Tuning.partch,"PartchOtonality1"),
	\partch_o2->Scale.new(#[0,7,13,18,27,35],43,
	Tuning.partch,"PartchOtonality2"),
	\partch_o3->Scale.new(#[0,6,12,21,29,36],43,
	Tuning.partch,"PartchOtonality3"),
	\partch_o4->Scale.new(#[0,5,15,23,30,37],43,
	Tuning.partch,"PartchOtonality4"),
	\partch_o5->Scale.new(#[0,10,18,25,31,38],43,
	Tuning.partch,"PartchOtonality5"),
	\partch_o6->Scale.new(#[0,9,16,22,28,33],43,
	Tuning.partch,"PartchOtonality6"),
	\partch_u1->Scale.new(#[0,9,18,23,29,35],43,
	Tuning.partch,"PartchUtonality1"),
	\partch_u2->Scale.new(#[0,8,16,25,30,36],43,
	Tuning.partch,"PartchUtonality2"),
	\partch_u3->Scale.new(#[0,7,14,22,31,37],43,
	Tuning.partch,"PartchUtonality3"),
	\partch_u4->Scale.new(#[0,6,13,20,28,38],43,
	Tuning.partch,"PartchUtonality4"),
	\partch_u5->Scale.new(#[0,5,12,18,25,33],43,
	Tuning.partch,"PartchUtonality5"),
	\partch_u6->Scale.new(#[0,10,15,21,27,34],43,
	Tuning.partch,"PartchUtonality6"),
	*/

	//hexatonicmodeswithnotritone
	new_scale("hexMajor7", (double[]){0,2,4,7,9,11,-1});
	new_scale("hexDorian", (double[]){0,2,3,5,7,10,-1});
	new_scale("hexPhrygian", (double[]){0,1,3,5,8,10,-1});
	new_scale("hexSus", (double[]){0,2,5,7,9,10,-1});
	new_scale("hexMajor6", (double[]){0,2,4,5,7,9,-1});
	new_scale("hexAeolian", (double[]){0,3,5,7,8,10,-1});

	//7notescales
	new_scale("major", (double[]){0,2,4,5,7,9,11,-1});
	new_scale("ionian", (double[]){0,2,4,5,7,9,11,-1});
	new_scale("dorian", (double[]){0,2,3,5,7,9,10,-1});
	new_scale("phrygian", (double[]){0,1,3,5,7,8,10,-1});
	new_scale("lydian", (double[]){0,2,4,6,7,9,11,-1});
	new_scale("mixolydian", (double[]){0,2,4,5,7,9,10,-1});
	new_scale("aeolian", (double[]){0,2,3,5,7,8,10,-1});
	new_scale("minor", (double[]){0,2,3,5,7,8,10,-1});
	new_scale("locrian", (double[]){0,1,3,5,6,8,10,-1});

	new_scale("harmonicMinor", (double[]){0,2,3,5,7,8,11,-1});
	new_scale("harmonicMajor", (double[]){0,2,4,5,7,8,11,-1});

	new_scale("melodicMinor", (double[]){0,2,3,5,7,9,11,-1});
	new_scale("melodicMinorDescending", (double[]){0,2,3,5,7,8,10,-1});
	new_scale("melodicMajor", (double[]){0,2,4,5,7,8,10,-1});

	new_scale("bartok", (double[]){0,2,4,5,7,8,10,-1});
	new_scale("hindu", (double[]){0,2,4,5,7,8,10,-1});

	//ragamodes
	new_scale("todi", (double[]){0,1,3,6,7,8,11,-1});
	new_scale("purvi", (double[]){0,1,4,6,7,8,11,-1});
	new_scale("marva", (double[]){0,1,4,6,7,9,11,-1});
	new_scale("bhairav", (double[]){0,1,4,5,7,8,11,-1});
	new_scale("ahirbhairav", (double[]){0,1,4,5,7,9,10,-1});

	new_scale("superLocrian", (double[]){0,1,3,4,6,8,10,-1});
	new_scale("romanianMinor", (double[]){0,2,3,6,7,9,10,-1});
	new_scale("hungarianMinor", (double[]){0,2,3,6,7,8,11,-1});
	new_scale("neapolitanMinor", (double[]){0,1,3,5,7,8,11,-1});
	new_scale("enigmatic", (double[]){0,1,4,6,8,10,11,-1});
	new_scale("spanish", (double[]){0,1,4,5,7,8,10,-1});

	//modesofwholetoneswithaddednote->
	new_scale("leadingWhole", (double[]){0,2,4,6,8,10,11,-1});
	new_scale("lydianMinor", (double[]){0,2,4,6,7,8,10,-1});
	new_scale("neapolitanMajor", (double[]){0,1,3,5,7,9,11,-1});
	new_scale("locrianMajor", (double[]){0,2,4,5,6,8,10,-1});

	//8notescales
	new_scale("diminished", (double[]){0,1,3,4,6,7,9,10,-1});
	new_scale("diminished2", (double[]){0,2,3,5,6,8,9,11,-1});

	//12notescales
	//\chromatic->Scale.new((0..11),12,name:"Chromatic"),

	//TWENTY-FOURTONESPEROCTAVE

	//\chromatic24->Scale.new((0..23),24,name:"Chromatic24"),

	/*
	//maqamajam
	\ajam->Scale.new(#[0,4,8,10,14,18,22],24,name:"Ajam"),
	\jiharkah->Scale.new(#[0,4,8,10,14,18,21],24,name:"Jiharkah"),
	\shawqAfza->Scale.new(#[0,4,8,10,14,16,22],24,name:"ShawqAfza"),

	//maqamsikah
	\sikah->Scale.new(#[0,3,7,11,14,17,21],24,name:"Sikah"),
	\sikahDesc->Scale.new(#[0,3,7,11,13,17,21],24,name:"SikahDescending"),
	\huzam->Scale.new(#[0,3,7,9,15,17,21],24,name:"Huzam"),
	\iraq->Scale.new(#[0,3,7,10,13,17,21],24,name:"Iraq"),
	\bastanikar->Scale.new(#[0,3,7,10,13,15,21],24,name:"Bastanikar"),
	\mustar->Scale.new(#[0,5,7,11,13,17,21],24,name:"Mustar"),

	//maqambayati
	\bayati->Scale.new(#[0,3,6,10,14,16,20],24,name:"Bayati"),
	\karjighar->Scale.new(#[0,3,6,10,12,18,20],24,name:"Karjighar"),
	\husseini->Scale.new(#[0,3,6,10,14,17,21],24,name:"Husseini"),

	//maqamnahawand
	\nahawand->Scale.new(#[0,4,6,10,14,16,22],24,name:"Nahawand"),
	\nahawandDesc->Scale.new(#[0,4,6,10,14,16,20],24,name:"NahawandDescending"),
	\farahfaza->Scale.new(#[0,4,6,10,14,16,20],24,name:"Farahfaza"),
	\murassah->Scale.new(#[0,4,6,10,12,18,20],24,name:"Murassah"),
	\ushaqMashri->Scale.new(#[0,4,6,10,14,17,21],24,name:"UshaqMashri"),

	//maqamrast
	\rast->Scale.new(#[0,4,7,10,14,18,21],24,name:"Rast"),
	\rastDesc->Scale.new(#[0,4,7,10,14,18,20],24,name:"RastDescending"),
	\suznak->Scale.new(#[0,4,7,10,14,16,22],24,name:"Suznak"),
	\nairuz->Scale.new(#[0,4,7,10,14,17,20],24,name:"Nairuz"),
	\yakah->Scale.new(#[0,4,7,10,14,18,21],24,name:"Yakah"),
	\yakahDesc->Scale.new(#[0,4,7,10,14,18,20],24,name:"YakahDescending"),
	\mahur->Scale.new(#[0,4,7,10,14,18,22],24,name:"Mahur"),

	//maqamhijaz
	\hijaz->Scale.new(#[0,2,8,10,14,17,20],24,name:"Hijaz"),
	\hijazDesc->Scale.new(#[0,2,8,10,14,16,20],24,name:"HijazDescending"),
	\zanjaran->Scale.new(#[0,2,8,10,14,18,20],24,name:"Zanjaran"),

	//maqamhijazKar
	\hijazKar->Scale.new(#[0,2,8,10,14,16,22],24,name:"hijazKar"),

	//maqamsaba
	\saba->Scale.new(#[0,3,6,8,12,16,20],24,name:"Saba"),
	\zamzam->Scale.new(#[0,2,6,8,14,16,20],24,name:"Zamzam"),

	//maqamkurd
	\kurd->Scale.new(#[0,2,6,10,14,16,20],24,name:"Kurd"),
	\kijazKarKurd->Scale.new(#[0,2,8,10,14,16,22],24,name:"KijazKarKurd"),

	//maqamnawaAthar
	\nawaAthar->Scale.new(#[0,4,6,12,14,16,22],24,name:"NawaAthar"),
	\nikriz->Scale.new(#[0,4,6,12,14,18,20],24,name:"Nikriz"),
	\atharKurd->Scale.new(#[0,2,6,12,14,16,22],24,name:"AtharKurd"),


	//Ascending/descendingscales
	\melodicMinor->ScaleAD(#[0,2,3,5,7,9,11],12,#[0,2,3,5,7,8,10],name:"MelodicMinor"),
	\sikah->ScaleAD(#[0,3,7,11,14,17,21],24,#[0,3,7,11,13,17,21],name:"Sikah"),
	\nahawand->ScaleAD(#[0,4,6,10,14,16,22],24,#[0,4,6,10,14,16,20],name:"Nahawand"),
	*/

	lua_setglobal(l, "scale");

}
