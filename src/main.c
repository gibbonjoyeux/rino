/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

// UDP: https://www.programminglogic.com/sockets-programming-in-c-using-udp-datagrams/
// OSC: https://opensoundcontrol.stanford.edu/spec-1_0-examples.html#OSCstrings
// OSC: http://wosclib.sourceforge.net/osc-ref.pdf

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
play_init(char *path) {
	time_t		t;

	mem_clean(&r, sizeof(r));
	str_cpy(r.path, path);
	r.time.beat = 0;
	r.time.bpm = BPM_DEFAULT;
	r.loops = NULL;
	r.loops_prev = NULL;
	r.loop = NULL;
	srand((unsigned)time(&t));
	/// INIT DATA POOLS
	pool_init(&(r.pool.loop), sizeof(t_loop));
	pool_init(&(r.pool.event), sizeof(t_event));
	pool_init(&(r.pool.call_param), sizeof(t_call_param));
}

static void
play_loop(void) {
	t_time		t1, t2;
	t_list		*list;
	int			beat_round, beat_round_prev;
	double		beat_scale;
	double		time_diff;
	t_time_mod	file_time;
	struct stat	file_stats;

	/// INIT TIME
	r.time.beat = 0;
	beat_round = 0;
	beat_round_prev = 0;
	gettimeofday(&(r.time.time_start), NULL);
	t2 = r.time.time_start;
	/// RUN LOOPS
	while (true) {
		/// UPDATE LOOPS
		list = r.loops;
		while (list != NULL) {
			r.loop = (t_loop*)&(list->content);
			loop_compute(r.loop);
			list = list->next;
		}
		r.loop = NULL;
		/// UPDATE BPM
		lua_getglobal(l, "BPM");
		if (lua_isnumber(l, -1) == true)
			r.time.bpm = lua_tonumber(l, -1);
		lua_pop(l, 1);
		/// UPDATE TIME
		beat_round = (int)r.time.beat;
		if (beat_round != beat_round_prev)
			r.time.new_beat = true;
		else
			r.time.new_beat = false;
		beat_round_prev = beat_round;
		gettimeofday(&t1, NULL);
		beat_scale = r.time.bpm / (double)60;
		time_diff = (t1.tv_sec - t2.tv_sec) * 1e6;
		time_diff = (time_diff + (t1.tv_usec - t2.tv_usec)) * 1e-6;
		r.time.beat += time_diff * beat_scale;
		t2 = t1;
		//printf("\e[1;1H\e[2J> %.2lf\n", r.time.beat);
		/// UPDATE FILE
		if (r.time.new_beat == true && stat(r.path, &file_stats) == 0) {
			file_time = *(gmtime(&file_stats.st_mtime));
			if (mem_cmp(&file_time, &(r.time.update), sizeof(struct tm)) != 0) {
				lua_load_file();
				r.time.update = file_time;
			}
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int					main(int argc, char **argv) {
	/// CHECK PARAMETERS
	if (argc < 2) {
		printf("Rino requires a file\n");
		return 1;
	}

	/// INIT
	play_init(argv[1]);
	server_init();
	if (lua_init() == false) {
		printf("\"%s\" file error\n", argv[1]);
		return 1;
	}
	if (lua_load_file() == false) {
		printf("\"%s\" file error\n", argv[1]);
		return 1;
	}
	/// LOOP
	play_loop();
	return 0;
}
