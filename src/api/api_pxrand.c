/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_pxrand(lua_State *l) {
	t_pattern	*pattern;
	size_t		length;
	int			value;

	/// CHECK INPUT
	if (lua_istable(l, 1) == false) {
		lua_pushnumber(l, 0);
		return 1;
	}
	/// COMPUTE PATTERN
	pattern = &(r.loop->pattern.list[r.loop->pattern.i]);
	length = lua_objlen(l, 1);
	value = random_range(0, length);
	if (length > 1)
		while (value == pattern->value)
			value = random_range(0, length);
	pattern->value = value;
	r.loop->pattern.i += 1;
	/// PUSH OUTPUT
	lua_rawgeti(l, 1, value + 1);
	return 1;
}
