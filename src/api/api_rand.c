/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_rand(lua_State *l) {
	int			length;
	double		lo, hi;

	if (lua_isnumber(l, 1) == true) {
		/// rand(lo, hi)
		if (lua_isnumber(l, 2) == true) {
			lo = lua_tonumber(l, 1);
			hi = lua_tonumber(l, 2);
			lua_pushnumber(l, random_range(lo, hi));
		/// rand(hi)
		} else {
			hi = lua_tonumber(l, 1);
			lua_pushnumber(l, random_range(0, hi));
		}
	/// rand(table)
	} else if (lua_istable(l, 1) == true) {
		length = lua_objlen(l, 1);
		lua_rawgeti(l, 1, (int)random_range(0, length) + 1);
	/// rand()
	} else {
		lua_pushnumber(l, random_range(0, 1));
	}
	return 1;
}
