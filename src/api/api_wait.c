/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
reset_last_wait(void) {
	t_list		*node;
	t_event		*event;

	node = r.loop->queue;
	while (node != NULL) {
		event = (t_event*)&(node->content);
		if (event->type == EVENT_WAIT)
			event->content.wait.last = false;
		node = node->next;
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_wait(lua_State *l) {
	t_list		*node;
	t_event		*event;
	double		delay;
	double		beat_inter;

	/// CREATE EVENT
	node = pool_get(&(r.pool.event));
	if (node == NULL)
		return 0;
	/// COMPUTE EVENT
	//// SET DELAY
	delay = lua_tonumber(l, 1);
	r.loop->beat += delay;
	//// ROUND DELAY (IF ON BEAT)
	beat_inter = r.loop->beat - (double)((int)r.loop->beat);
	if (ABS(beat_inter) < BEAT_PRECISION)
		r.loop->beat = ROUND(r.loop->beat);
	/// SET EVENT
	event = (t_event*)&(node->content);
	event->type = EVENT_WAIT;
	event->content.wait.type = EVENT_WAIT_REGULAR;
	event->content.wait.beat = r.loop->beat;
	event->content.wait.last = true;
	reset_last_wait();
	/// PUSH EVENT
	lst_append(&(r.loop->queue), node);
	return 0;
}
