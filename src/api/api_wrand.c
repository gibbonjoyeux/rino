/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_wrand(lua_State *l) {
	double		rand, rand_weight;
	int			length;
	int			i;

	/// CHECK INPUT
	if (lua_istable(l, 1) == false || lua_istable(l, 2) == false)
		return 0;
	length = lua_objlen(l, 1);
	if (length != (int)lua_objlen(l, 2))
		return 0;
	/// COMPUTE WEIGHT
	rand = random_range(0, 1);
	rand_weight = 0;
	for (i = 0; i < length; ++i) {
		lua_rawgeti(l, 2, i + 1);
		rand_weight += lua_tonumber(l, -1);
		if (rand < rand_weight) {
			lua_rawgeti(l, 1, i + 1);
			return 1;
		}
	}
	/// DEFAULT WEIGHT
	lua_rawgeti(l, 1, length);
	return 1;
}
