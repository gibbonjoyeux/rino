/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_play(lua_State *l) {
	t_list		*node;
	t_event		*event;
	t_call		*call;
	bool		list_state;
	int			type;

	/// CHECK INPUT
	if (lua_isstring(l, 1) == false || lua_istable(l, 2) == false)
		return 0;
	/// CREATE EVENT
	node = pool_get(&(r.pool.event));
	if (node == NULL)
		return 0;
	/// INIT EVENT
	event = (t_event*)&(node->content);
	event->type = EVENT_CALL;
	/// INIT EVENT CALL
	call = &(event->content.call);
	/// ADD USER PATH
	str_cpy(call->path, (char*)lua_tostring(l, 1));
	/// ADD USER PARAMETERS
	call->param = NULL;
	lua_pushnil(l);
	list_state = 0;
	/// KEY		index - 2
	/// VALUE	index - 1
	while (lua_next(l, 2) != 0) {
		/// MODE DICT
		if (lua_type(l, 2) == LUA_TSTRING) {
			type = lua_type(l, -1);
			if (type == LUA_TNUMBER) {
				call_param_new_string(call, (char*)lua_tostring(l, -2));
				call_param_new_float(call, lua_tonumber(l, -1));
			} else if (type == LUA_TSTRING) {
				call_param_new_string(call, (char*)lua_tostring(l, -2));
				call_param_new_string(call, (char*)lua_tostring(l, -1));
			}
		/// MODE LIST
		} else {
			/// INSERT KEY
			if (list_state == 0) {
				call_param_new_string(call, (char*)lua_tostring(l, -1));
			/// INSERT VALUE
			} else {
				type = lua_type(l, -1);
				if (type == LUA_TNUMBER)
					call_param_new_float(call, lua_tonumber(l, -1));
				else if (type == LUA_TSTRING)
					call_param_new_string(call, (char*)lua_tostring(l, -1));
			}
			list_state = !list_state;
		}
		lua_pop(l, 1);
	}
	/// PUSH EVENT
	lst_append(&(r.loop->queue), node);
	return 0;
}
