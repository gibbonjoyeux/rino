/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline bool
reset_last_wait(int beat_aim) {
	t_list		*node;
	t_event		*event;

	node = r.loop->queue;
	while (node != NULL) {
		event = (t_event*)&(node->content);
		/// LAST WAIT EVENT
		if (event->type == EVENT_WAIT && event->content.wait.last == true) {
			/// NOT LAST ANYMORE
			event->content.wait.last = false;
			if (event->content.wait.type == EVENT_WAIT_FOR) {
				/// TWO `wait_for` CANNOT GIVE THE SAME TIME
				if ((int)event->content.wait.beat == beat_aim)
					return true;
			}
			return false;
		}
		node = node->next;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_wait_for(lua_State *l) {
	t_list		*node;
	t_event		*event;
	int			beat_mod, beat_aim;
	double		beat_inter;

	beat_mod = lua_tonumber(l, 1);
	if (beat_mod <= 0 || beat_mod > 64)
		beat_mod = 1;
	/// CREATE EVENT
	node = pool_get(&(r.pool.event));
	if (node == NULL)
		return 0;
	/// INIT EVENT
	//// COMPUTE CURRENT ROUND BEAT
	beat_aim = (int)r.loop->beat;
	beat_inter = r.loop->beat - (double)beat_aim;
	if (ABS(beat_inter) >= BEAT_PRECISION)
		beat_aim += 1;
	//// COMPUTE ALIGNED BEAT
	while (beat_aim % beat_mod != 0)
		beat_aim += 1;
	if (reset_last_wait(beat_aim) == true)
		beat_aim += beat_mod;
	r.loop->beat = beat_aim;
	event = (t_event*)&(node->content);
	event->type = EVENT_WAIT;
	event->content.wait.type = EVENT_WAIT_FOR;
	event->content.wait.beat = r.loop->beat;
	event->content.wait.last = true;
	/// PUSH EVENT
	lst_append(&(r.loop->queue), node);
	return 0;
}
