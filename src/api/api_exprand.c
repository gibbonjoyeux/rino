/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline double
compute_exprand(double range) {
	/// log E exponential
	//return log(1.0 + (random_range(0, 1)) * (E - 1.0)) * range;
	/// log 10 exponential
	return log10(1.0 + (random_range(0, 1)) * (10.0 - 1.0)) * range;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_exprand(lua_State *l) {
	int			length;
	double		lo, hi;
	double		value;

	if (lua_isnumber(l, 1) == true) {
		/// rand(lo, hi)
		if (lua_isnumber(l, 2) == true) {
			lo = lua_tonumber(l, 1);
			hi = lua_tonumber(l, 2);
			value = lo + compute_exprand(hi - lo);
			lua_pushnumber(l, value);
		/// rand(hi)
		} else {
			hi = lua_tonumber(l, 1);
			value = compute_exprand(hi);
			lua_pushnumber(l, value);
		}
	/// rand(table)
	} else if (lua_istable(l, 1) == true) {
		length = lua_objlen(l, 1);
		value = compute_exprand(length);
		lua_rawgeti(l, 1, (int)value + 1);
	/// rand()
	} else {
		value = compute_exprand(1);
		lua_pushnumber(l, value);
	}
	return 1;
}
