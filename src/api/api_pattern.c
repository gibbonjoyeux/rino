/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

static int			build_pattern(void);
static inline bool	build_pattern_seq(t_pattern*);
static inline bool	build_pattern_wrand(t_pattern*);

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// BUILD
//////////////////////////////////////////////////

static inline bool
shuffle_check(t_pattern *pattern) {
	bool		buffer[BUFFER_PAT_DATA / 2];
	int			i;

	for (i = 0; i < pattern->length; ++i)
		buffer[i] = false;
	for (i = 0; i < pattern->length; ++i) {
		if (pattern->buffer[i * 2 + 1] < pattern->length)
			buffer[pattern->buffer[i * 2 + 1]] = true;
		else
			return false;
	}
	for (i = 0; i < pattern->length; ++i)
		if (buffer[i] == false)
			return false;
	return true;
}

static inline void
shuffle_make(t_pattern *pattern) {
	int			tmp;
	int			i, j;

	/// FILL BUFFER
	for (i = 0; i < pattern->length; ++i)
		pattern->buffer[i * 2 + 1] = i;
	/// SHUFFLE BUFFER
	for (i = 0; i < pattern->length; ++i) {
		j = (int)random_range(0, pattern->length);
		tmp = pattern->buffer[i * 2 + 1];
		pattern->buffer[i * 2 + 1] = pattern->buffer[j * 2 + 1];
		pattern->buffer[j * 2 + 1] = tmp;
	}
}

///
/// BUILD SEQUENCED PATTERN
///  buffer use: [float value, float value, ...]
/// -> build complete
///
static inline bool
build_pattern_seq(t_pattern *pattern) {
	int			child_index;
	int			i;

	/// GET CONTENT
	lua_rawgeti(l, -1, 3);
	if (lua_istable(l, -1) == false) {
		lua_pop(l, 1);
		return false;
	}
	pattern->length = lua_objlen(l, -1);
	lua_rawgeti(l, -1, 1);
	//// MODE REGULAR
	if (lua_isnumber(l, -1) == true) {
		lua_pop(l, 1);
		pattern->mode = PATTERN_REGULAR;
		for (i = 0; i < pattern->length; ++i) {
			lua_rawgeti(l, -1, i + 1);
			((float*)(pattern->buffer))[i] = lua_tonumber(l, -1);
			lua_pop(l, 1);
		}
	//// MODE RECURSIVE
	} else if (lua_istable(l, -1) == true) {
		lua_pop(l, 1);
		pattern->mode = PATTERN_RECURSIVE;
		for (i = 0; i < pattern->length; ++i) {
			/// BUILD CHILD
			lua_rawgeti(l, -1, i + 1);
			child_index = build_pattern();
			if (child_index < 0) {
				lua_pop(l, 2);
				return false;
			}
			/// LINK CHILD
			pattern->buffer[i] = child_index;
			lua_pop(l, 1);
		}
	//// MODE ERROR
	} else {
		lua_pop(l, 2);
		return false;
	}
	lua_pop(l, 1);
	return true;
}

///
/// BUILD WEIGHTED PATTERN
///  buffer use: [float value, float weight, float value, float weight...]
/// -> build complete
///
static inline bool
build_pattern_wrand(t_pattern *pattern) {
	int			child_index;
	int			i;

	/// GET CONTENT VALUE
	lua_rawgeti(l, -1, 3);
	if (lua_istable(l, -1) == false) {
		lua_pop(l, 1);
		return false;
	}
	/// GET CONTENT WEIGHT
	lua_rawgeti(l, -2, 4);
	if (lua_istable(l, -1) == false) {
		lua_pop(l, 2);
		return false;
	}
	pattern->length = lua_objlen(l, -2);
	if (pattern->length != (int)lua_objlen(l, -1)) {
		lua_pop(l, 2);
		return false;
	}
	lua_rawgeti(l, -2, 1);
	//// MODE REGULAR
	if (lua_isnumber(l, -1) == true) {
		lua_pop(l, 1);
		pattern->mode = PATTERN_REGULAR;
		for (i = 0; i < pattern->length; ++i) {
			/// GET VALUE
			lua_rawgeti(l, -2, i + 1);
			((float*)(pattern->buffer))[i * 2] = lua_tonumber(l, -1);
			lua_pop(l, 1);
			/// GET WEIGHT
			lua_rawgeti(l, -1, i + 1);
			((float*)(pattern->buffer))[i * 2 + 1] = lua_tonumber(l, -1);
			lua_pop(l, 1);
		}
	//// MODE RECURSIVE
	} else if (lua_istable(l, -1) == true) {
		lua_pop(l, 1);
		pattern->mode = PATTERN_RECURSIVE;
		for (i = 0; i < pattern->length; ++i) {
			/// GET VALUE
			//// BUILD CHILD
			lua_rawgeti(l, -2, i + 1);
			child_index = build_pattern();
			if (child_index < 0) {
				lua_pop(l, 2);
				return false;
			}
			//// LINK CHILD
			pattern->buffer[i * 2] = child_index;
			lua_pop(l, 1);
			/// GET WEIGHT
			lua_rawgeti(l, -1, i + 1);
			((float*)(pattern->buffer))[i * 2 + 1] = lua_tonumber(l, -1);
			lua_pop(l, 1);
		}
	//// MODE ERROR
	} else {
		lua_pop(l, 3);
		return false;
	}
	lua_pop(l, 2);
	return true;
}

///
/// BUILD SHUFFLED PATTERN
///  buffer use: [float value, int order, float value, int order...]
/// -> build complete
///
static inline bool
build_pattern_shuf(t_pattern *pattern) {
	int			child_index;
	int			i;

	/// GET CONTENT
	lua_rawgeti(l, -1, 3);
	if (lua_istable(l, -1) == false) {
		lua_pop(l, 1);
		return false;
	}
	pattern->length = lua_objlen(l, -1);
	lua_rawgeti(l, -1, 1);
	//// MODE REGULAR
	if (lua_isnumber(l, -1) == true) {
		lua_pop(l, 1);
		pattern->mode = PATTERN_REGULAR;
		for (i = 0; i < pattern->length; ++i) {
			lua_rawgeti(l, -1, i + 1);
			((float*)(pattern->buffer))[i * 2] = lua_tonumber(l, -1);
			lua_pop(l, 1);
		}
		if (shuffle_check(pattern) == false)
			shuffle_make(pattern);
	//// MODE RECURSIVE
	} else if (lua_istable(l, -1) == true) {
		lua_pop(l, 1);
		pattern->mode = PATTERN_RECURSIVE;
		pattern->length = lua_objlen(l, -1);
		for (i = 0; i < pattern->length; ++i) {
			/// BUILD CHILD
			lua_rawgeti(l, -1, i + 1);
			child_index = build_pattern();
			if (child_index < 0) {
				lua_pop(l, 2);
				return false;
			}
			/// LINK CHILD
			pattern->buffer[i * 2] = child_index;
			lua_pop(l, 1);
		}
		if (shuffle_check(pattern) == false)
			shuffle_make(pattern);
	//// MODE ERROR
	} else {
		lua_pop(l, 2);
		return false;
	}
	lua_pop(l, 1);
	return true;
}

///
/// BUILD PATTERN TREE (RECUSRIVE)
/// -> error
///
static int
build_pattern(void) {
	t_pattern	*pattern;
	char		*type;
	int			pattern_index;

	pattern_index = r.loop->pattern.i;
	pattern = &(r.loop->pattern.list[pattern_index]);
	r.loop->pattern.i += 1;
	//if (lua_objlen(l, -1) != 3)
	//	return -1;
	/// GET TYPE
	lua_rawgeti(l, -1, 1);
	if (lua_isstring(l, -1) == false) {
		lua_pop(l, 1);
		return -1;
	}
	type = (char*)lua_tostring(l, -1);
	if (str_equ(type, "seq") == true) {
		pattern->type = PATTERN_SEQ;
	} else if (str_equ(type, "nseq") == true) {
		pattern->type = PATTERN_NSEQ;
	} else if (str_equ(type, "rand") == true) {
		pattern->type = PATTERN_RAND;
	} else if (str_equ(type, "xrand") == true) {
		pattern->type = PATTERN_XRAND;
	} else if (str_equ(type, "wrand") == true) {
		pattern->type = PATTERN_WRAND;
	} else if (str_equ(type, "shuf") == true) {
		pattern->type = PATTERN_SHUF;
	} else if (str_equ(type, "nshuf") == true) {
		pattern->type = PATTERN_NSHUF;
	} else {
		return -1;
		lua_pop(l, 1);
	}
	lua_pop(l, 1);
	/// GET LENGTH
	lua_rawgeti(l, -1, 2);
	if (lua_isnumber(l, -1) == false) {
		lua_pop(l, 1);
		return -1;
	}
	pattern->count = lua_tonumber(l, -1);
	lua_pop(l, 1);
	/// GET CONTENT
	switch(pattern->type) {
		case PATTERN_SEQ:
		case PATTERN_NSEQ:
		case PATTERN_RAND:
		case PATTERN_XRAND:
			if (build_pattern_seq(pattern) == false)
				return -1;
			break;
		case PATTERN_WRAND:
			if (build_pattern_wrand(pattern) == false)
				return -1;
			break;
		case PATTERN_SHUF:
		case PATTERN_NSHUF:
			if (build_pattern_shuf(pattern) == false)
				return -1;
			break;
	}
	return pattern_index;
}

//////////////////////////////////////////////////
/// COMPUTE
//////////////////////////////////////////////////

///
/// COMPUTE PATTERN TREE (RECURSIVE)
/// -> is pattern ended
///
static bool
compute_pattern(int pattern_index) {
	t_pattern	*pattern;
	double		rnd;
	int			i;

	pattern = &(r.loop->pattern.list[pattern_index]);
	switch(pattern->type) {
		case PATTERN_SEQ:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				lua_pushnumber(l, ((float*)(pattern->buffer))[i]);
				pattern->a += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				if (compute_pattern(pattern->buffer[i]) == true)
					pattern->a += 1;
			}
			/// COMPUTE INDEX
			if (pattern->a >= pattern->length) {
				pattern->a = 0;
				pattern->b += 1;
				if (pattern->b >= pattern->count) {
					pattern->b = 0;
					return true;
				}
			}
		break;
		case PATTERN_NSEQ:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				lua_pushnumber(l, ((float*)(pattern->buffer))[i]);
				pattern->a += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				if (compute_pattern(pattern->buffer[i]) == true)
					pattern->a += 1;
			}
			/// COMPUTE INDEX
			if (pattern->a >= pattern->count) {
				pattern->a = 0;
				return true;
			}
		break;
		case PATTERN_RAND:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				lua_pushnumber(l, ((float*)(pattern->buffer))[i]);
				pattern->a = random_range(0, pattern->length);
				pattern->b += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				if (compute_pattern(pattern->buffer[i]) == true) {
					pattern->a = random_range(0, pattern->length);
					pattern->b += 1;
				}
			}
			/// COMPUTE INDEX
			if (pattern->b >= pattern->count) {
				pattern->b = 0;
				return true;
			}
		break;
		case PATTERN_XRAND:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				lua_pushnumber(l, ((float*)(pattern->buffer))[i]);
				pattern->a = random_range(0, pattern->length);
				if (pattern->length > 1)
					while (pattern->a == i)
						pattern->a = random_range(0, pattern->length);
				pattern->b += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				if (compute_pattern(pattern->buffer[i]) == true) {
					if (pattern->length > 1)
						while (pattern->a == i)
							pattern->a = random_range(0, pattern->length);
					pattern->b += 1;
				}
			}
			/// COMPUTE INDEX
			if (pattern->b >= pattern->count) {
				pattern->b = 0;
				return true;
			}
		break;
		case PATTERN_WRAND:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				lua_pushnumber(l, ((float*)(pattern->buffer))[i * 2]);
				/// PRE-COMPUTE NEXT VALUE
				rnd = random_range(0, 1);
				for (i = 0; i < pattern->length; ++i) {
					rnd -= ((float*)(pattern->buffer))[i * 2 + 1];
					if (rnd <= 0)
						break;
				}
				if (i == pattern->length)
					i = pattern->length - 1;
				pattern->a = i;
				pattern->b += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				if (compute_pattern(pattern->buffer[i * 2]) == true) {
					/// PRE-COMPUTE NEXT VALUE
					rnd = random_range(0, 1);
					for (i = 0; i < pattern->length; ++i) {
						rnd -= ((float*)(pattern->buffer))[i * 2 + 1];
						if (rnd <= 0)
							break;
					}
					if (i == pattern->length)
						i = pattern->length - 1;
					pattern->a = i;
					pattern->b += 1;
				}
			}
			/// COMPUTE INDEX
			if (pattern->b >= pattern->count) {
				pattern->b = 0;
				return true;
			}
		break;
		case PATTERN_SHUF:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				i = pattern->buffer[i * 2 + 1];
				lua_pushnumber(l, ((float*)(pattern->buffer))[i * 2]);
				pattern->a += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				i = pattern->buffer[i * 2 + 1];
				if (compute_pattern(pattern->buffer[i * 2]) == true)
					pattern->a += 1;
			}
			/// COMPUTE INDEX
			if (pattern->a >= pattern->length) {
				pattern->a = 0;
				pattern->b += 1;
				if (pattern->b >= pattern->count) {
					pattern->b = 0;
					shuffle_make(pattern);
					return true;
				}
			}
		break;
		case PATTERN_NSHUF:
			/// COMPUTE VALUE
			if (pattern->mode == PATTERN_REGULAR) {
				i = pattern->a % pattern->length;
				i = pattern->buffer[i * 2 + 1];
				lua_pushnumber(l, ((float*)(pattern->buffer))[i * 2]);
				pattern->a += 1;
			} else if (pattern->mode == PATTERN_RECURSIVE) {
				i = pattern->a % pattern->length;
				i = pattern->buffer[i * 2 + 1];
				if (compute_pattern(pattern->buffer[i * 2]) == true)
					pattern->a += 1;
			}
			/// COMPUTE INDEX
			if (pattern->a >= pattern->count) {
				pattern->a = 0;
				shuffle_make(pattern);
				return true;
			}
		break;
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_pattern(lua_State *l) {
	//t_pattern	*pattern;
	int			pattern_index;

	/// CHECK INPUT
	if (lua_istable(l, 1) == false)
		return 0;
	/// COMPUTE PATTERN
	pattern_index = r.loop->pattern.i;
	if (build_pattern() < 0) {
		printf("PATTERN ERROR\n");
		r.loop->pattern.i = pattern_index;
		return 0;
	}
	compute_pattern(pattern_index);
	return 1;
}
