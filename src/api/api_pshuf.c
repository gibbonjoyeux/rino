/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

static inline void
shuffle_buffer(t_pattern *pattern, int length) {
	bool		buffer[32];
	bool		shuffled;
	int			tmp;
	int			i, j;

	shuffled = true;
	for (i = 0; i < length; ++i)
		buffer[i] = false;
	for (i = 0; i < length; ++i)
		if (pattern->buffer[i] < length)
			buffer[pattern->buffer[i]] = true;
	for (i = 0; i < length; ++i)
		if (buffer[i] == false)
			shuffled = false;
	/// SHUFFLE
	if (shuffled == false) {
		/// FILL BUFFER
		for (i = 0; i < length; ++i)
			pattern->buffer[i] = i;
		/// SHUFFLE BUFFER
		for (i = 0; i < length; ++i) {
			j = (int)random_range(0, length);
			tmp = pattern->buffer[i];
			pattern->buffer[i] = pattern->buffer[j];
			pattern->buffer[j] = tmp;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

int
api_pshuf(lua_State *l) {
	t_pattern	*pattern;
	int			length;
	int			value;

	/// CHECK INPUT
	if (lua_istable(l, 1) == false)
		return 0;
	/// GET PATTERN
	pattern = &(r.loop->pattern.list[r.loop->pattern.i]);
	length = lua_objlen(l, 1);
	/// SHUFFLE PATTERN
	shuffle_buffer(pattern, length);
	/// COMPUTE OUTPUT
	value = pattern->value % length;
	pattern->value = (pattern->value + 1) % length;
	r.loop->pattern.i += 1;
	/// PUSH OUTPUT
	lua_rawgeti(l, 1, pattern->buffer[value] + 1);
	return 1;
}
