/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "rino.h"

// UDP: https://www.programminglogic.com/sockets-programming-in-c-using-udp-datagrams/
// OSC: https://opensoundcontrol.stanford.edu/spec-1_0-examples.html#OSCstrings
// OSC: http://wosclib.sourceforge.net/osc-ref.pdf

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void
server_init(void) {
	/// INIT SOCKET
	r.osc.client_socket = socket(PF_INET, SOCK_DGRAM, 0);
	/// INIT SERVER
	mem_clean(&(r.osc.server), sizeof(r.osc.server));
	r.osc.server.sin_family = AF_INET;
	r.osc.server.sin_port = htons(PORT_SCSYNTH);
	r.osc.server.sin_addr.s_addr = inet_addr("127.0.0.1");
	/// BIND SERVER
	bind(r.osc.client_socket, (t_sockaddr*)&r.osc.server, sizeof(r.osc.server));
}
