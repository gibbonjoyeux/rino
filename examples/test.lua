
NOTES = scale.minorPentatonic
NOTES = {
	NOTES[1], NOTES[2], NOTES[3], NOTES[4],
	NOTES[5], NOTES[1], NOTES[2], NOTES[3]
}
BPM		= 60
OUT		= 0

--function	loop_test()
--	local	t, t_aim
--	local	delay
--	local	freq
--
--	freq = 440
--	t = time()
--	t_aim = floor(t) + 1
--	while true do
--		scplay("bell", {
--			"freq", freq,
--			"atk", 0,
--			"rel", 0.1,
--			"amp", 0.1
--		})
--		freq = 220
--
--		--delay = rand(0.01, 0.5)
--		delay = 0.01 * floor(rand(1, 60))
--		if t + delay >= t_aim then
--			delay = t_aim - t
--			wait(delay)
--			return
--		else
--			t = t + delay
--			wait(delay)
--		end
--	end
--end

CLICK_AMP = 1
function	loop_click()
	local	freq, oct
	local	dur
	local	amp
	local	pan

	-- INIT TIME
	if CLICK_TIME == nil then CLICK_TIME = 0 end
	-- PLAY
	freq = midi_cps(69 + pxrand(NOTES), 400)
	oct = rand({1, 1, 2, 4})
	dur = p({
		"xrand", 1, {
			{"shuf",		1,	{0.5, 0.25, 0.25}},
			{"shuf",		4,	{0.25, 0.25, 0.25, 0.125, 0.125}},
			{"seq",			16,	{1/8}},
			{"seq",			3,	{1/3}}
		}
	})
	if CLICK_TIME == 0 then
		amp = CLICK_AMP
		pan = 0
		oct = 4
	else
		amp = rand(1.125, 2.5)
		amp = rand(CLICK_AMP * 0.25, CLICK_AMP * 0.5)
		pan = rand(-1, 1)
	end
	scplay("tick", {
		"out", OUT,
		"freq", freq * oct,
		"pan", pan,
		"rel", rand(0.001, 0.01),
		"amp", CLICK_AMP
	})
	wait(dur)
	-- UPDATE TIME
	CLICK_TIME = CLICK_TIME + dur
	if CLICK_TIME > 0.99 then
		CLICK_TIME = 0
	end

	print(time())
end
