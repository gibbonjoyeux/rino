
BPM = 60

function	loop_pattern()
	local	freq

	--pattern({
	--	"seq", 1,
	--	{220, 330, 440}
	--})
	--freq = p({
	--	"seq", 1,
	--	{
	--		{"seq", 3, {220}},
	--		{"seq", 3, {330}},
	--		{"seq", 1, {440, 550, 660}}
	--	}
	--})
	--freq = p({
	--	"wrand", 1,
	--	{
	--		{"seq", 3, {220}},
	--		{"seq", 3, {330}},
	--		{"seq", 3, {440}}
	--	}, {0.5, 0.3, 0.2}
	--})
	freq = p({
		"shuf", 3,
		{
			{"seq", 4, {220}},
			{"seq", 4, {330}},
			{"seq", 4, {440}}
		}
	})

	scplay("bell", {"freq", freq})

	wait(0.25)
end

function	loop_beat()
	wait_for(1)
	scplay("bell", {"freq", 110})
end
