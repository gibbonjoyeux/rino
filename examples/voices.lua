
BPM				= 60
NOTES			= scale.chinese
OUT				= 0
OUT_REVERB		= 4

VOICES_NUM		= 120
VOICES_TIME_MAX	= 120
-- CONTINUOUS AMBIANT VOICES
function	loop_ambiant()
	local	freq, oct
	local	atk, rel, total
	local	i

	-- INIT VOICES
	if VOICES == nil then
		VOICES = {}
		for i = 1, VOICES_NUM do
			VOICES[i] = map(i, 1, VOICES_NUM, 0, VOICES_TIME_MAX)
		end
	end
	-- UPDATE VOICES
	for i = 1, #VOICES do
		VOICES[i] = VOICES[i] - 0.1
		if VOICES[i] < 0 then
			atk = floor(rand(0, 6) * 10) / 10
			rel = floor(rand(0, 6) * 10) / 10
			oct = rand({0.25, 0.5, 1, 2})
			freq = midi_cps(69 + pxrand(NOTES), 400) * oct
			VOICES[i] = atk + rel
			scplay("melo_noise", {
				"out", OUT_REVERB,
				"freq", freq,
				"freq_mod_freq", 0,
				"freq_mod_amp", 0,
				"rq", rand(0.01, 0.001),
				"atk", atk,
				"rel", rel,
				"pan", rand(-1, 1),
				"amp", 2
			})
			--scplay("melo_noise", {
			--	"out", OUT,
			--	"freq", freq,
			--	"freq_mod_freq", 0,
			--	"freq_mod_amp", 0,
			--	"rq", rand(0.01, 0.001),
			--	"atk", atk,
			--	"rel", rel,
			--	"pan", rand(-1, 1),
			--	"amp", 1
			--})
		end
	end
	wait(0.1)
end

WEIRD_AMP = 1.5
function	loop_ambiant_high()
	local	freq
	local	atk
	local	rel

	wait_for(12)
	freq = midi_cps(69 + pxrand(NOTES), 400) * 4
	atk = rand(1, 6)
	rel = rand(1, 6)
	scplay("melo_noise", {
		"out", OUT_REVERB,
		"freq", freq,
		"freq_mod_freq", 2,
		"freq_mod_amp", 32,
		"rq", rand(0.01, 0.001),
		"atk", atk,
		"rel", rel,
		"pan", 0,
		"amp", WEIRD_AMP
	})
end

BASS_AMP = 0.1
function	loop_bass()
	local	freq
	local	amp

	wait_for(12)
	freq = midi_cps(69 - 24 + pxrand(NOTES), 400)
	scplay("bass", {
		"freq", freq,
		"freq_mod_freq", 0.25,
		"freq_mod_amp", 2,
		"pan", rand(-0.3, 0.3),
		"amp", BASS_AMP
	})
end

function	loop_kick()
	local	dur
	local	mix

	-- INIT AMP
	if KICK_AMP == nil then KICK_AMP = 0 end
	if KICK_TIME == nil then KICK_TIME = 0 end
	mix = map(sin(KICK_TIME * 0.1), -1, 1, 0, 1)
	-- PLAY
	dur = pseq({0.4, 0.6, 1})
	scplay("kick", {
		"freq", 60,
		"mix", mix,
		"amp", KICK_AMP
	})
	wait(dur)
	-- UPDATE KICK_AMP
	if KICK_AMP < 1 then
		KICK_AMP = KICK_AMP + 1 / 240
	end
	KICK_TIME = KICK_TIME + dur
end

CLICK_AMP = 1
function	loop_click()
	local	freq, oct
	local	dur
	local	amp
	local	pan

	-- INIT TIME
	if CLICK_TIME == nil then CLICK_TIME = 0 end
	-- PLAY
	freq = midi_cps(69 + pxrand(NOTES), 400)
	oct = rand({1, 1, 2, 4})
	dur = p({
		"xrand", 1, {
			{"shuf",		1,	{0.5, 0.25, 0.25}},
			{"shuf",		4,	{0.25, 0.25, 0.25, 0.125, 0.125}},
			{"seq",			16,	{1/8}},
			{"seq",			3,	{1/3}},
			{"shuf",		1,	{
				{"seq",		1,	{0.5}},
				{"seq",		8,	{1/16}},
			}}
		}
	})
	if CLICK_TIME == 0 then
		amp = 5
		pan = 0
		oct = 4
	else
		amp = rand(1.125, 2.5)
		pan = rand(-1, 1)
	end
	scplay("tick", {
		"freq", freq * oct,
		"pan", pan,
		"rel", rand(0.001, 0.01),
		"amp", amp * CLICK_AMP
	})
	wait(dur)
	-- UPDATE TIME
	CLICK_TIME = CLICK_TIME + dur
	if CLICK_TIME > 0.99 then
		CLICK_TIME = 0
	end
end
