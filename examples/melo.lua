
BPM = 60
NOTES = scale.chinese

function	loop_ambiant()
	local	freq
	local	oct
	local	i

	for i = 1, 6 do
		oct = rand({0.5, 1, 2})
		freq = midi_cps(69 + pxrand(NOTES)) * oct
		scplay("melo_noise", {
			"freq", freq,
			"rq", rand(0.01, 0.001),
			"atk", rand(3, 6),
			"rel", rand(3, 6),
			"pan", rand(-1, 1),
			"amp", 5
		})
	end
	wait(6)
end

function	loop_bass()
	local	freq
	local	amp

	freq = midi_cps(69 - 24 + pxrand(NOTES))
	scplay("bass", {
		"freq", freq,
		"pan", rand(-1, 1),
		"amp", 0.1
	})
	wait(12)
end

function	loop_kick()
	local	dur

	dur = pseq({0.4, 0.6, 1})
	scplay("kick", {
		"freq", 60,
		"mix", 0,
		"amp", 0.125
	})
	wait(dur)
end

t = 0
function	loop_click()
	local	freq
	local	dur
	local	amp
	local	pan

	freq = midi_cps(69 + pxrand(NOTES)) * rand({1, 1, 2})
	dur = p({
		"xrand", 1, {
			{"shuf",		1,	{0.5, 0.25, 0.25}},
			{"shuf",		4,	{0.25, 0.25, 0.25, 0.125, 0.125}},
			{"seq",			16,	{1/8}},
			{"seq",			3,	{1/3}},
			{"seq",			2,	{
				{"seq",		1,	{0.5}},
				{"seq",		8,	{1/16}},
			}}
		}
	})
	if t == 0 then
		amp = 5
		pan = 0
	else
		amp = rand(1.125, 2.5)
		pan = rand(-1, 1)
	end

	scplay("tick", {
		"freq", freq,
		"pan", pan,
		"rel", rand(0.001, 0.01),
		"amp", amp
	})
	wait(dur)

	t = t + dur
	if t > 0.99 then
		t = 0
	end
end
