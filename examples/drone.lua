
BPM = 60
NOTES = scale.chinese
NOTES = {
	NOTES[1], NOTES[2], NOTES[3], NOTES[4],
	NOTES[5], NOTES[1], NOTES[2], NOTES[3]
}
REVERB = 4

BASS_AMP = 0.3
function	loop_bass()
	local	freq
	local	amp
	local	lpf_mix, lpf_freq

	freq = midi_cps(69 - 24 + pxrand(NOTES)) * 0.25
	lpf_mix = 1
	lpf_freq = freq * 32
	-- LEFT CHANNEL
	scplay("wt_drone", {
		"out", REVERB,
		"freq", freq,
		"freq_mod_freq", 0.5,
		"freq_mod_amp", 0.5,
		"table_freq", 0.1,
		"lpf_mix", lpf_mix,
		"lpf_freq", lpf_freq,
		"atk", 1,
		"sus", 4,
		"rel", 2,
		"pan", -1,
		"amp", BASS_AMP
	})
	-- RIGHT CHANNEL
	scplay("wt_drone", {
		"out", REVERB,
		"freq", freq * 1.001,
		"freq_mod_freq", 0.5,
		"freq_mod_amp", 0.5,
		"table_freq", 0.1,
		"lpf_mix", lpf_mix,
		"lpf_freq", lpf_freq,
		"atk", 1,
		"sus", 4,
		"rel", 2,
		"pan", 1,
		"amp", BASS_AMP
	})
	-- WAIT
	wait(6)
end

SAD_AMP = 0.05
function	loop_sad()
	local	note
	local	oct
	local	freq
	local	pan
	local	lpf_mix, lpf_freq

	-- COMPUTE FRE
	note = p({
		"xrand", 1, {
			{"xrand", 8, NOTES},
			{"seq", 4, NOTES},
			{"shuf", 4, NOTES}
		}
	})
	oct = pwrand({1, 2}, {0.9, 0.1})
	freq = midi_cps(69 - 12 + note) * oct
	-- INIT PARAM
	lpf_mix = rand(0, 1)
	lpf_freq = lerp(lpf_mix, freq * 0.5, 20000)
	pan = rand(-1, 1)
	-- PLAY WET
	scplay("wt_drone", {
		"out", REVERB,
		"freq", freq,
		"freq_mod_freq", 2,
		"freq_mod_amp", 2,
		"table_freq", 0.1,
		"lpf_mix", lpf_mix,
		"lpf_freq", lpf_freq,
		"atk", 0,
		"sus", 0,
		"rel", 1,
		"pan", pan,
		"amp", SAD_AMP
	})
	-- PLAY DRY
	scplay("wt_drone", {
		"out", 0,
		"freq", freq,
		"freq_mod_freq", 2,
		"freq_mod_amp", 2,
		"table_freq", 0.1,
		"lpf_mix", lpf_mix,
		"lpf_freq", lpf_freq,
		"atk", 0,
		"sus", 0,
		"rel", 1,
		"pan", pan,
		"amp", SAD_AMP
	})
	-- WAIT
	wait(0.25)
end
