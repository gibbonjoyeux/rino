
BPM = 60
NOTES = scale.minorPentatonic
NOTES = {
	NOTES[1], NOTES[2], NOTES[3], NOTES[4],
	NOTES[5], NOTES[1], NOTES[2], NOTES[3]
}
OUT = 0
OUT_REVERB = 4

CHANCE_LEAD = 1
MIX_LEAD = 1
MIX_MAGIC = 0
MIX_AMBIANT = 0
MIX_LOW_MAX = 0
MIX_LOW = 0
MIX_CLICK = 0
TIME_STEP = 0.1
function	loop_event()
	if T == nil then T = 0 end

	MIX_LEAD = 1
	MIX_MAGIC = map(T, 8, 16, 0, 1, true)
	MIX_AMBIANT = map(T, 8, 16, 0, 1, true)
	MIX_LOW_MAX = map(T, 16, 40, 0, 1, true)
	MIX_LOW = map(T, 16, 40, 0, 1, true)
	MIX_CLICK = map(T, 32, 40, 0, 1, true)

	CHANCE_LEAD = map(T, 32, 48, 1, 0.7, true)

	if T > 90 then print("END") end

	T = T + TIME_STEP
	wait(TIME_STEP)
end

LEAD_AMP = 0.02
function	loop_lead()
	local	freq
	local	dur

	freq = p({ "shuf", 4, NOTES })
	freq = midi_cps(48 + freq, 400) * 4
	scplay("bell", {
		"out", OUT,
		"freq", freq,
		"atk", 0,
		"rel", 0.3,
		"amp", 0.02 * MIX_LEAD
	})
	dur = wrand({0.25, 0.125, 0.5}, {CHANCE_LEAD, 0.2, 0.1})
	wait(dur)
end

MAGIC_AMP = 0.02
function	loop_MAGIC()
	local	freq
	local	dur

	freq = pxrand(NOTES)
	freq = midi_cps(48 + freq, 400) * 8
	scplay("bell", {
		"out", OUT_REVERB,
		"freq", freq,
		"atk", 0,
		"rel", 0.3,
		"amp", 0.02 * MIX_MAGIC
	})
	scplay("bell", {
		"out", 0,
		"freq", freq,
		"atk", 0,
		"rel", 0.3,
		"amp", 0.02 * MIX_MAGIC
	})
	dur = rand(3, 8)
	wait(dur)
end

AMBIANT_AMP = 0.01
function	loop_ambiant()
	local	freq
	local	octave

	-- NOTE LOW
	freq = midi_cps(36 + pxrand(NOTES), 400)
	octave = pseq({0.5, 1, 2, 4})
	scplay( "bell", {
		"out", OUT_REVERB,
		"freq", freq * octave * 2,
		"amp", AMBIANT_AMP * MIX_AMBIANT * 0.5
	} )
	wait( 0.25 )
	-- NOTE HIGH
	freq = midi_cps(48 + pxrand(NOTES), 400)
	octave = pseq({0.5, 1, 2, 4, 8})
	scplay( "bell", {
		"out", OUT_REVERB,
		"freq", freq * octave,
		"amp", AMBIANT_AMP * MIX_AMBIANT
	} )
	wait( 0.25 )
end

LOW_AMP_MAX = 0.25
LOW_AMP = 0.3
function	loop_lead_low()
	local	freq
	local	amp
	local	i
	local	r

	wait_for( 1 )

	scplay( "bell", {
		"freq", midi_cps(48 + NOTES[1], 400),
		"rel", 2,
		"amp", LOW_AMP_MAX * MIX_LOW
	} )
	wait( 0.25 )
	for i = 1, 7 do
		freq = midi_cps(48 + pxrand(NOTES), 400)
		freq = freq * wrand({ 1, 2, 4 }, { 0.7, 0.2, 0.1 })
		amp = rand(LOW_AMP)
		scplay( "bell", {
			--"out", OUT_REVERB,
			"freq", freq,
			"rel", 0.1,
			"amp", LOW_AMP * MIX_LOW
		} )
		wait( 0.25 )
	end
end

CLICK_AMP = 1
function	loop_click()
	local	freq, oct
	local	dur
	local	amp
	local	pan

	-- INIT TIME
	if CLICK_TIME == nil then CLICK_TIME = 0 end
	-- PLAY
	freq = midi_cps(69 + pxrand(NOTES), 400)
	oct = rand({1, 1, 2, 4})
	dur = p({
		"xrand", 1, {
			{"shuf",		1,	{0.5, 0.25, 0.25}},
			{"shuf",		4,	{0.25, 0.25, 0.25, 0.125, 0.125}},
			{"seq",			16,	{1/8}},
			{"seq",			3,	{1/3}},
			{"shuf",		1,	{
				{"seq",		1,	{0.5}},
				{"seq",		8,	{1/16}},
			}}
		}
	})
	if CLICK_TIME == 0 then
		amp = CLICK_AMP
		pan = 0
		oct = 4
	else
		amp = rand(1.125, 2.5)
		amp = rand(CLICK_AMP * 0.25, CLICK_AMP * 0.5)
		pan = rand(-1, 1)
	end
	scplay("tick", {
		--"out", OUT_REVERB,
		"freq", freq * oct,
		"pan", pan,
		"rel", rand(0.001, 0.01),
		"amp", CLICK_AMP * MIX_CLICK
	})
	wait(dur)
	-- UPDATE TIME
	CLICK_TIME = CLICK_TIME + dur
	if CLICK_TIME > 0.99 then
		CLICK_TIME = 0
	end
end
